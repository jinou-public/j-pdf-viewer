import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JPdfViewerTestModule } from '../../../test.module';
import { PdfUpdateComponent } from 'app/entities/pdf/pdf-update.component';
import { PdfService } from 'app/entities/pdf/pdf.service';
import { Pdf } from 'app/shared/model/pdf.model';

describe('Component Tests', () => {
  describe('Pdf Management Update Component', () => {
    let comp: PdfUpdateComponent;
    let fixture: ComponentFixture<PdfUpdateComponent>;
    let service: PdfService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JPdfViewerTestModule],
        declarations: [PdfUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PdfUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PdfUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PdfService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Pdf(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Pdf();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
