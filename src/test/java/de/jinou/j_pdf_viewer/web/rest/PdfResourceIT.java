package de.jinou.j_pdf_viewer.web.rest;

import de.jinou.j_pdf_viewer.JPdfViewerApp;
import de.jinou.j_pdf_viewer.domain.Pdf;
import de.jinou.j_pdf_viewer.repository.PdfRepository;
import de.jinou.j_pdf_viewer.service.PdfService;
import de.jinou.j_pdf_viewer.service.dto.PdfDTO;
import de.jinou.j_pdf_viewer.service.mapper.PdfMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PdfResource} REST controller.
 */
@SpringBootTest(classes = JPdfViewerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PdfResourceIT {

    private static final byte[] DEFAULT_PDFDATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PDFDATA = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PDFDATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PDFDATA_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_COURSE = "AAAAAAAAAA";
    private static final String UPDATED_COURSE = "BBBBBBBBBB";

    @Autowired
    private PdfRepository pdfRepository;

    @Autowired
    private PdfMapper pdfMapper;

    @Autowired
    private PdfService pdfService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPdfMockMvc;

    private Pdf pdf;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pdf createEntity(EntityManager em) {
        Pdf pdf = new Pdf()
            .pdfdata(DEFAULT_PDFDATA)
            .pdfdataContentType(DEFAULT_PDFDATA_CONTENT_TYPE)
            .title(DEFAULT_TITLE)
            .course(DEFAULT_COURSE);
        return pdf;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pdf createUpdatedEntity(EntityManager em) {
        Pdf pdf = new Pdf()
            .pdfdata(UPDATED_PDFDATA)
            .pdfdataContentType(UPDATED_PDFDATA_CONTENT_TYPE)
            .title(UPDATED_TITLE)
            .course(UPDATED_COURSE);
        return pdf;
    }

    @BeforeEach
    public void initTest() {
        pdf = createEntity(em);
    }

    @Test
    @Transactional
    public void createPdf() throws Exception {
        int databaseSizeBeforeCreate = pdfRepository.findAll().size();
        // Create the Pdf
        PdfDTO pdfDTO = pdfMapper.toDto(pdf);
        restPdfMockMvc.perform(post("/api/pdfs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pdfDTO)))
            .andExpect(status().isCreated());

        // Validate the Pdf in the database
        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeCreate + 1);
        Pdf testPdf = pdfList.get(pdfList.size() - 1);
        assertThat(testPdf.getPdfdata()).isEqualTo(DEFAULT_PDFDATA);
        assertThat(testPdf.getPdfdataContentType()).isEqualTo(DEFAULT_PDFDATA_CONTENT_TYPE);
        assertThat(testPdf.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testPdf.getCourse()).isEqualTo(DEFAULT_COURSE);
    }

    @Test
    @Transactional
    public void createPdfWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pdfRepository.findAll().size();

        // Create the Pdf with an existing ID
        pdf.setId(1L);
        PdfDTO pdfDTO = pdfMapper.toDto(pdf);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPdfMockMvc.perform(post("/api/pdfs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pdfDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pdf in the database
        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = pdfRepository.findAll().size();
        // set the field null
        pdf.setTitle(null);

        // Create the Pdf, which fails.
        PdfDTO pdfDTO = pdfMapper.toDto(pdf);


        restPdfMockMvc.perform(post("/api/pdfs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pdfDTO)))
            .andExpect(status().isBadRequest());

        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPdfs() throws Exception {
        // Initialize the database
        pdfRepository.saveAndFlush(pdf);

        // Get all the pdfList
        restPdfMockMvc.perform(get("/api/pdfs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pdf.getId().intValue())))
            .andExpect(jsonPath("$.[*].pdfdataContentType").value(hasItem(DEFAULT_PDFDATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].pdfdata").value(hasItem(Base64Utils.encodeToString(DEFAULT_PDFDATA))))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].course").value(hasItem(DEFAULT_COURSE)));
    }
    
    @Test
    @Transactional
    public void getPdf() throws Exception {
        // Initialize the database
        pdfRepository.saveAndFlush(pdf);

        // Get the pdf
        restPdfMockMvc.perform(get("/api/pdfs/{id}", pdf.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pdf.getId().intValue()))
            .andExpect(jsonPath("$.pdfdataContentType").value(DEFAULT_PDFDATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.pdfdata").value(Base64Utils.encodeToString(DEFAULT_PDFDATA)))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.course").value(DEFAULT_COURSE));
    }
    @Test
    @Transactional
    public void getNonExistingPdf() throws Exception {
        // Get the pdf
        restPdfMockMvc.perform(get("/api/pdfs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePdf() throws Exception {
        // Initialize the database
        pdfRepository.saveAndFlush(pdf);

        int databaseSizeBeforeUpdate = pdfRepository.findAll().size();

        // Update the pdf
        Pdf updatedPdf = pdfRepository.findById(pdf.getId()).get();
        // Disconnect from session so that the updates on updatedPdf are not directly saved in db
        em.detach(updatedPdf);
        updatedPdf
            .pdfdata(UPDATED_PDFDATA)
            .pdfdataContentType(UPDATED_PDFDATA_CONTENT_TYPE)
            .title(UPDATED_TITLE)
            .course(UPDATED_COURSE);
        PdfDTO pdfDTO = pdfMapper.toDto(updatedPdf);

        restPdfMockMvc.perform(put("/api/pdfs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pdfDTO)))
            .andExpect(status().isOk());

        // Validate the Pdf in the database
        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeUpdate);
        Pdf testPdf = pdfList.get(pdfList.size() - 1);
        assertThat(testPdf.getPdfdata()).isEqualTo(UPDATED_PDFDATA);
        assertThat(testPdf.getPdfdataContentType()).isEqualTo(UPDATED_PDFDATA_CONTENT_TYPE);
        assertThat(testPdf.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testPdf.getCourse()).isEqualTo(UPDATED_COURSE);
    }

    @Test
    @Transactional
    public void updateNonExistingPdf() throws Exception {
        int databaseSizeBeforeUpdate = pdfRepository.findAll().size();

        // Create the Pdf
        PdfDTO pdfDTO = pdfMapper.toDto(pdf);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPdfMockMvc.perform(put("/api/pdfs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pdfDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pdf in the database
        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePdf() throws Exception {
        // Initialize the database
        pdfRepository.saveAndFlush(pdf);

        int databaseSizeBeforeDelete = pdfRepository.findAll().size();

        // Delete the pdf
        restPdfMockMvc.perform(delete("/api/pdfs/{id}", pdf.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pdf> pdfList = pdfRepository.findAll();
        assertThat(pdfList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
