package de.jinou.j_pdf_viewer.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.jinou.j_pdf_viewer.web.rest.TestUtil;

public class PdfTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pdf.class);
        Pdf pdf1 = new Pdf();
        pdf1.setId(1L);
        Pdf pdf2 = new Pdf();
        pdf2.setId(pdf1.getId());
        assertThat(pdf1).isEqualTo(pdf2);
        pdf2.setId(2L);
        assertThat(pdf1).isNotEqualTo(pdf2);
        pdf1.setId(null);
        assertThat(pdf1).isNotEqualTo(pdf2);
    }
}
