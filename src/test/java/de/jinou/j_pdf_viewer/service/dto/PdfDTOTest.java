package de.jinou.j_pdf_viewer.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.jinou.j_pdf_viewer.web.rest.TestUtil;

public class PdfDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PdfDTO.class);
        PdfDTO pdfDTO1 = new PdfDTO();
        pdfDTO1.setId(1L);
        PdfDTO pdfDTO2 = new PdfDTO();
        assertThat(pdfDTO1).isNotEqualTo(pdfDTO2);
        pdfDTO2.setId(pdfDTO1.getId());
        assertThat(pdfDTO1).isEqualTo(pdfDTO2);
        pdfDTO2.setId(2L);
        assertThat(pdfDTO1).isNotEqualTo(pdfDTO2);
        pdfDTO1.setId(null);
        assertThat(pdfDTO1).isNotEqualTo(pdfDTO2);
    }
}
