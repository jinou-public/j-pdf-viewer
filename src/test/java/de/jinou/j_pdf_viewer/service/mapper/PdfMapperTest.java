package de.jinou.j_pdf_viewer.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PdfMapperTest {

    private PdfMapper pdfMapper;

    @BeforeEach
    public void setUp() {
        pdfMapper = new PdfMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(pdfMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pdfMapper.fromId(null)).isNull();
    }
}
