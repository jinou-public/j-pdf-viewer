import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPdf } from 'app/shared/model/pdf.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { PdfService } from './pdf.service';
import { PdfDeleteDialogComponent } from './pdf-delete-dialog.component';

@Component({
  selector: 'jhi-pdf',
  templateUrl: './pdf.component.html',
})
export class PdfComponent implements OnInit, OnDestroy {
  pdfs: IPdf[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected pdfService: PdfService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.pdfs = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.pdfService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IPdf[]>) => this.paginatePdfs(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.pdfs = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPdfs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPdf): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInPdfs(): void {
    this.eventSubscriber = this.eventManager.subscribe('pdfListModification', () => this.reset());
  }

  delete(pdf: IPdf): void {
    const modalRef = this.modalService.open(PdfDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pdf = pdf;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginatePdfs(data: IPdf[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.pdfs.push(data[i]);
      }
    }
  }
}
