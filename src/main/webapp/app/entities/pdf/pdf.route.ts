import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPdf, Pdf } from 'app/shared/model/pdf.model';
import { PdfService } from './pdf.service';
import { PdfComponent } from './pdf.component';
import { PdfDetailComponent } from './pdf-detail.component';
import { PdfUpdateComponent } from './pdf-update.component';

@Injectable({ providedIn: 'root' })
export class PdfResolve implements Resolve<IPdf> {
  constructor(private service: PdfService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPdf> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((pdf: HttpResponse<Pdf>) => {
          if (pdf.body) {
            return of(pdf.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Pdf());
  }
}

export const pdfRoute: Routes = [
  {
    path: '',
    component: PdfComponent,
    data: {
      pageTitle: 'Pdfs',
    },
  },
  {
    path: ':id/view',
    component: PdfDetailComponent,
    resolve: {
      pdf: PdfResolve,
    },
    data: {
      pageTitle: 'Pdfs',
    },
  },
  {
    path: 'new',
    component: PdfUpdateComponent,
    resolve: {
      pdf: PdfResolve,
    },
    data: {
      pageTitle: 'Pdfs',
    },
  },
  {
    path: ':id/edit',
    component: PdfUpdateComponent,
    resolve: {
      pdf: PdfResolve,
    },
    data: {
      pageTitle: 'Pdfs',
    },
  },
];
