import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IPdf, Pdf } from 'app/shared/model/pdf.model';
import { PdfService } from './pdf.service';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-pdf-update',
  templateUrl: './pdf-update.component.html',
})
export class PdfUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    pdfdata: [],
    pdfdataContentType: [],
    title: [null, [Validators.required]],
    course: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected pdfService: PdfService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pdf }) => {
      this.updateForm(pdf);
    });
  }

  updateForm(pdf: IPdf): void {
    this.editForm.patchValue({
      id: pdf.id,
      pdfdata: pdf.pdfdata,
      pdfdataContentType: pdf.pdfdataContentType,
      title: pdf.title,
      course: pdf.course,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('jPdfViewerApp.error', { message: err.message })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pdf = this.createFromForm();
    if (pdf.id !== undefined) {
      this.subscribeToSaveResponse(this.pdfService.update(pdf));
    } else {
      this.subscribeToSaveResponse(this.pdfService.create(pdf));
    }
  }

  private createFromForm(): IPdf {
    return {
      ...new Pdf(),
      id: this.editForm.get(['id'])!.value,
      pdfdataContentType: this.editForm.get(['pdfdataContentType'])!.value,
      pdfdata: this.editForm.get(['pdfdata'])!.value,
      title: this.editForm.get(['title'])!.value,
      course: this.editForm.get(['course'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPdf>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
