import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPdf } from 'app/shared/model/pdf.model';
import { PdfService } from './pdf.service';

@Component({
  templateUrl: './pdf-delete-dialog.component.html',
})
export class PdfDeleteDialogComponent {
  pdf?: IPdf;

  constructor(protected pdfService: PdfService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.pdfService.delete(id).subscribe(() => {
      this.eventManager.broadcast('pdfListModification');
      this.activeModal.close();
    });
  }
}
