import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JPdfViewerSharedModule } from 'app/shared/shared.module';
import { PdfComponent } from './pdf.component';
import { PdfDetailComponent } from './pdf-detail.component';
import { PdfUpdateComponent } from './pdf-update.component';
import { PdfDeleteDialogComponent } from './pdf-delete-dialog.component';
import { pdfRoute } from './pdf.route';
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {JPdfViewerAppRoutingModule} from "../../app-routing.module";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";

@NgModule({
  imports: [JPdfViewerSharedModule, RouterModule.forChild(pdfRoute),
    PdfViewerModule, NgxExtendedPdfViewerModule],
  declarations: [PdfComponent, PdfDetailComponent, PdfUpdateComponent, PdfDeleteDialogComponent],
  entryComponents: [PdfDeleteDialogComponent],
})
export class JPdfViewerPdfModule {}
platformBrowserDynamic().bootstrapModule(JPdfViewerPdfModule);
