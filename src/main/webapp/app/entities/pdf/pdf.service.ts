import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPdf } from 'app/shared/model/pdf.model';

type EntityResponseType = HttpResponse<IPdf>;
type EntityArrayResponseType = HttpResponse<IPdf[]>;

@Injectable({ providedIn: 'root' })
export class PdfService {
  public resourceUrl = SERVER_API_URL + 'api/pdfs';

  constructor(protected http: HttpClient) {}

  create(pdf: IPdf): Observable<EntityResponseType> {
    return this.http.post<IPdf>(this.resourceUrl, pdf, { observe: 'response' });
  }

  update(pdf: IPdf): Observable<EntityResponseType> {
    return this.http.put<IPdf>(this.resourceUrl, pdf, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPdf>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPdf[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
