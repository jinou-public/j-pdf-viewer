export interface IPdf {
  id?: number;
  pdfdataContentType?: string;
  pdfdata?: any;
  title?: string;
  course?: string;
}

export class Pdf implements IPdf {
  constructor(
    public id?: number,
    public pdfdataContentType?: string,
    public pdfdata?: any,
    public title?: string,
    public course?: string
  ) {}
}
