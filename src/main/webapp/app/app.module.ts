import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { JPdfViewerSharedModule } from 'app/shared/shared.module';
import { JPdfViewerCoreModule } from 'app/core/core.module';
import { JPdfViewerAppRoutingModule } from './app-routing.module';
import { JPdfViewerHomeModule } from './home/home.module';
import { JPdfViewerEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { ViewerComponent } from './viewer/viewer.component';
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {PdfViewerModule} from "ng2-pdf-viewer";

@NgModule({
  imports: [
    BrowserModule,
    JPdfViewerSharedModule,
    JPdfViewerCoreModule,
    JPdfViewerHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    JPdfViewerEntityModule,
    JPdfViewerAppRoutingModule,
    PdfViewerModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent, ViewerComponent],
  bootstrap: [MainComponent],
})
export class JPdfViewerAppModule {}
platformBrowserDynamic().bootstrapModule(JPdfViewerAppModule);
