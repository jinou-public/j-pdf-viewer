import { Component, OnInit } from '@angular/core';

class PDFService {
}

@Component({
  selector: 'jhi-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {

  pdfSrc = '2010_Book_BestPracticeSoftware-Engineeri.pdf';

  constructor(private pdfService: PDFService) { }

  ngOnInit(): void {
  }

}
