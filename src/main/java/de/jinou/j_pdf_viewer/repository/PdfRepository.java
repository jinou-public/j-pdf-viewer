package de.jinou.j_pdf_viewer.repository;

import de.jinou.j_pdf_viewer.domain.Pdf;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Pdf entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PdfRepository extends JpaRepository<Pdf, Long> {
}
