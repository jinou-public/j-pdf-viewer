package de.jinou.j_pdf_viewer.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Pdf.
 */
@Entity
@Table(name = "pdf")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Pdf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "pdfdata")
    private byte[] pdfdata;

    @Column(name = "pdfdata_content_type")
    private String pdfdataContentType;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "course")
    private String course;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPdfdata() {
        return pdfdata;
    }

    public Pdf pdfdata(byte[] pdfdata) {
        this.pdfdata = pdfdata;
        return this;
    }

    public void setPdfdata(byte[] pdfdata) {
        this.pdfdata = pdfdata;
    }

    public String getPdfdataContentType() {
        return pdfdataContentType;
    }

    public Pdf pdfdataContentType(String pdfdataContentType) {
        this.pdfdataContentType = pdfdataContentType;
        return this;
    }

    public void setPdfdataContentType(String pdfdataContentType) {
        this.pdfdataContentType = pdfdataContentType;
    }

    public String getTitle() {
        return title;
    }

    public Pdf title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCourse() {
        return course;
    }

    public Pdf course(String course) {
        this.course = course;
        return this;
    }

    public void setCourse(String course) {
        this.course = course;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pdf)) {
            return false;
        }
        return id != null && id.equals(((Pdf) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pdf{" +
            "id=" + getId() +
            ", pdfdata='" + getPdfdata() + "'" +
            ", pdfdataContentType='" + getPdfdataContentType() + "'" +
            ", title='" + getTitle() + "'" +
            ", course='" + getCourse() + "'" +
            "}";
    }
}
