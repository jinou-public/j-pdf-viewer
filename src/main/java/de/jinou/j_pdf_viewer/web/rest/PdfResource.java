package de.jinou.j_pdf_viewer.web.rest;

import de.jinou.j_pdf_viewer.service.PdfService;
import de.jinou.j_pdf_viewer.web.rest.errors.BadRequestAlertException;
import de.jinou.j_pdf_viewer.service.dto.PdfDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link de.jinou.j_pdf_viewer.domain.Pdf}.
 */
@RestController
@RequestMapping("/api")
public class PdfResource {

    private final Logger log = LoggerFactory.getLogger(PdfResource.class);

    private static final String ENTITY_NAME = "pdf";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PdfService pdfService;

    public PdfResource(PdfService pdfService) {
        this.pdfService = pdfService;
    }

    /**
     * {@code POST  /pdfs} : Create a new pdf.
     *
     * @param pdfDTO the pdfDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pdfDTO, or with status {@code 400 (Bad Request)} if the pdf has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pdfs")
    public ResponseEntity<PdfDTO> createPdf(@Valid @RequestBody PdfDTO pdfDTO) throws URISyntaxException {
        log.debug("REST request to save Pdf : {}", pdfDTO);
        if (pdfDTO.getId() != null) {
            throw new BadRequestAlertException("A new pdf cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PdfDTO result = pdfService.save(pdfDTO);
        return ResponseEntity.created(new URI("/api/pdfs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pdfs} : Updates an existing pdf.
     *
     * @param pdfDTO the pdfDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pdfDTO,
     * or with status {@code 400 (Bad Request)} if the pdfDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pdfDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pdfs")
    public ResponseEntity<PdfDTO> updatePdf(@Valid @RequestBody PdfDTO pdfDTO) throws URISyntaxException {
        log.debug("REST request to update Pdf : {}", pdfDTO);
        if (pdfDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PdfDTO result = pdfService.save(pdfDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pdfDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pdfs} : get all the pdfs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pdfs in body.
     */
    @GetMapping("/pdfs")
    public ResponseEntity<List<PdfDTO>> getAllPdfs(Pageable pageable) {
        log.debug("REST request to get a page of Pdfs");
        Page<PdfDTO> page = pdfService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pdfs/:id} : get the "id" pdf.
     *
     * @param id the id of the pdfDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pdfDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pdfs/{id}")
    public ResponseEntity<PdfDTO> getPdf(@PathVariable Long id) {
        log.debug("REST request to get Pdf : {}", id);
        Optional<PdfDTO> pdfDTO = pdfService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pdfDTO);
    }

    /**
     * {@code DELETE  /pdfs/:id} : delete the "id" pdf.
     *
     * @param id the id of the pdfDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pdfs/{id}")
    public ResponseEntity<Void> deletePdf(@PathVariable Long id) {
        log.debug("REST request to delete Pdf : {}", id);
        pdfService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
