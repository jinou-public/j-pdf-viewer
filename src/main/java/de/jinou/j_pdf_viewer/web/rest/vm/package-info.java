/**
 * View Models used by Spring MVC REST controllers.
 */
package de.jinou.j_pdf_viewer.web.rest.vm;
