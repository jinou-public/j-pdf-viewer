package de.jinou.j_pdf_viewer.service.impl;

import de.jinou.j_pdf_viewer.service.PdfService;
import de.jinou.j_pdf_viewer.domain.Pdf;
import de.jinou.j_pdf_viewer.repository.PdfRepository;
import de.jinou.j_pdf_viewer.service.dto.PdfDTO;
import de.jinou.j_pdf_viewer.service.mapper.PdfMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Pdf}.
 */
@Service
@Transactional
public class PdfServiceImpl implements PdfService {

    private final Logger log = LoggerFactory.getLogger(PdfServiceImpl.class);

    private final PdfRepository pdfRepository;

    private final PdfMapper pdfMapper;

    public PdfServiceImpl(PdfRepository pdfRepository, PdfMapper pdfMapper) {
        this.pdfRepository = pdfRepository;
        this.pdfMapper = pdfMapper;
    }

    @Override
    public PdfDTO save(PdfDTO pdfDTO) {
        log.debug("Request to save Pdf : {}", pdfDTO);
        Pdf pdf = pdfMapper.toEntity(pdfDTO);
        pdf = pdfRepository.save(pdf);
        return pdfMapper.toDto(pdf);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PdfDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pdfs");
        return pdfRepository.findAll(pageable)
            .map(pdfMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PdfDTO> findOne(Long id) {
        log.debug("Request to get Pdf : {}", id);
        return pdfRepository.findById(id)
            .map(pdfMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pdf : {}", id);
        pdfRepository.deleteById(id);
    }
}
