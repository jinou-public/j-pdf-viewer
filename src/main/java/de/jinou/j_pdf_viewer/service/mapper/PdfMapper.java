package de.jinou.j_pdf_viewer.service.mapper;


import de.jinou.j_pdf_viewer.domain.*;
import de.jinou.j_pdf_viewer.service.dto.PdfDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pdf} and its DTO {@link PdfDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PdfMapper extends EntityMapper<PdfDTO, Pdf> {



    default Pdf fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pdf pdf = new Pdf();
        pdf.setId(id);
        return pdf;
    }
}
