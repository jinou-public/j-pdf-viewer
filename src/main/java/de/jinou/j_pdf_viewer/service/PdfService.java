package de.jinou.j_pdf_viewer.service;

import de.jinou.j_pdf_viewer.service.dto.PdfDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link de.jinou.j_pdf_viewer.domain.Pdf}.
 */
public interface PdfService {

    /**
     * Save a pdf.
     *
     * @param pdfDTO the entity to save.
     * @return the persisted entity.
     */
    PdfDTO save(PdfDTO pdfDTO);

    /**
     * Get all the pdfs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PdfDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pdf.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PdfDTO> findOne(Long id);

    /**
     * Delete the "id" pdf.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
