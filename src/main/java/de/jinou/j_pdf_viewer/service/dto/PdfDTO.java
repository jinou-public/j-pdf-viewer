package de.jinou.j_pdf_viewer.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link de.jinou.j_pdf_viewer.domain.Pdf} entity.
 */
public class PdfDTO implements Serializable {
    
    private Long id;

    @Lob
    private byte[] pdfdata;

    private String pdfdataContentType;
    @NotNull
    private String title;

    private String course;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPdfdata() {
        return pdfdata;
    }

    public void setPdfdata(byte[] pdfdata) {
        this.pdfdata = pdfdata;
    }

    public String getPdfdataContentType() {
        return pdfdataContentType;
    }

    public void setPdfdataContentType(String pdfdataContentType) {
        this.pdfdataContentType = pdfdataContentType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PdfDTO)) {
            return false;
        }

        return id != null && id.equals(((PdfDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PdfDTO{" +
            "id=" + getId() +
            ", pdfdata='" + getPdfdata() + "'" +
            ", title='" + getTitle() + "'" +
            ", course='" + getCourse() + "'" +
            "}";
    }
}
